import { useState } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import UserContext from './UserContext';

//React router dom is a package that allows routing to be done by the client

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Login from './pages/Login';
// import NotFound from './pages/NotFound';
import Register from './pages/Register';


//use destructuring to isolate only the react-bootstrap components that we need


//App entry point
//JSX

export default function App() {
  // Creating a user state from App component provides a single source of truth. 

    const [user, setUser] = useState({
      token: localStorage.getItem('token')
    }); 

    // Function to be used for logging out. 
    const unsetUser = () => {
      localStorage.clear();
      setUser({ token: null });
    }

    return (
        <UserContext.Provider value={{ user, setUser, unsetUser }}>
            <BrowserRouter>
              <AppNavbar/>
              <Routes>
                  <Route exact path="/" element={<Home/>}/>
                  <Route exact path="/register" element={<Register/>}/>
                  <Route exact path="/login" element={<Login/>}/>
                  {/*<Route path="*" element={<NotFound/>}/>*/}
                </Routes>
            </BrowserRouter>
        </UserContext.Provider>
    );
}
