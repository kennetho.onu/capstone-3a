import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default function Banner() {
	return (
		<Row>
			<Col>
				<Jumbotron>
					<h1>Nunu Capitals</h1>
					<p>Opportunities for everyone, everywhere</p>
					<Button>Enquire now!</Button>
				</Jumbotron>
			</Col>
		</Row>
	);
}