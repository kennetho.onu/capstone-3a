import { useState, useEffect } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

//Regarding events:
//Click: Happens when a user clicks on an element
//Submit: Happens when the form button is clicked OR when the user pressed the enter key once all inputs are filled

//Keypress: Happens when a user presses on a keyboard key
//Change: Happens during ANY changes in an input value

export default function Register(){

	const [firstName, setFirstName] =  useState("")
	const [lastName, setLastName] =  useState("")
	const [email, setEmail] =  useState("")
	const [mobileNo, setMobileNo] =  useState("")
	const [password1, setPassword1] =  useState("")
	const [password2, setPassword2] =  useState("")
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {
		if((firstName !== "" && lastName !== "" && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [firstName, lastName, email, mobileNo, password1, password2])

	const registerUser = (e) => {
		e.preventDefault()

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Duplicate email found",
					icon: 'error',
					text: "Please provide a different email"
				})
			}else{
				fetch('http://localhost:4000/users/register', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(res => res.json())
				.then(data => {

					if(data){
						Swal.fire({
							title: "Registration successful",
							icon: 'success',
							text: 'You have been successfully registerd'
						})

						setFirstName("")
						setLastName("")
						setEmail("")
						setMobileNo("")
						setPassword1("")
						setPassword2("")
					}else{
						Swal.fire({
							title: "Registration failed",
							icon: 'error',
							text: 'You have not been registerd'
						})
					}
				})
			}
		})		
	}

	return(
		<Container>
			<Form className="mt-3" onSubmit={e => registerUser(e)}>
				<Form.Group controlId="firstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control type="text" placeholder="Enter your first name" required value={firstName} onChange={e => setFirstName(e.target.value)}/>
					{/*e.target.value = "The event's target's current value*/}
				</Form.Group>

				<Form.Group controlId="lastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control type="text" placeholder="Enter your last name" required value={lastName} onChange={e => setLastName(e.target.value)}/>
				</Form.Group>

				<Form.Group controlId="email">
					<Form.Label>Email Address</Form.Label>
					<Form.Control type="email" placeholder="Enter your email" required value={email} onChange={e => setEmail(e.target.value)}/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group controlId="mobileNo">
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control type="text" placeholder="Enter your mobile number" required value={mobileNo} onChange={e => setMobileNo(e.target.value)}/>
				</Form.Group>

				<Form.Group controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Enter your password" required value={password1} onChange={e => setPassword1(e.target.value)}/>
				</Form.Group>

				<Form.Group controlId="password2">
					<Form.Label>Verify Password</Form.Label>
					<Form.Control type="password" placeholder="Verify your password" required value={password2} onChange={e => setPassword2(e.target.value)}/>
				</Form.Group>

				{(isActive)
					?
					<Button variant="primary" type="submit">Submit</Button>
					:
					<Button variant="primary" disabled>Submit</Button>
				}
			</Form>
		</Container>
	)
}